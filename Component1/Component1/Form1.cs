﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Component1
{
    public partial class Form1 : Form
    {
        Graphics g;
        private int x;
        private int y;
        private Color red = Color.Red;
        private Color blue = Color.Blue;
        private Color yellow = Color.Yellow;
        private object element;

        public Form1()
        {
            InitializeComponent();
            pictureBox1.Image = new Bitmap(Size.Width, Size.Height); //allows the graphics to be visible
        }

        private void runBtn_Click(object sender, EventArgs e)
        {
            String input = textBox1.Text;
            if (input.Trim() == "")
            {
                MessageBox.Show("Enter a command", "ERROR");
            }

            else
            {
                String[] eachLine;
                String[] splitLines;
                ArrayList currentLine = new ArrayList();
                int i = 0;
                //textBox1.Text.
                if (input.ToLower().Trim() == "run")
                {
                    splitLines = richTextBox1.Lines.ToArray();
                    while (splitLines.Length != i)
                    {
                        eachLine = splitLines[i].Split(' ');
                        currentLine.Add(eachLine);
                        i++;
                    }
                }

                else
                {
                    splitLines = textBox1.Lines.ToArray();
                    eachLine = textBox1.Text.Split(' ');
                    currentLine.Add(eachLine);
                }
                InputCheck(currentLine, splitLines);
                pictureBox1.Refresh();

            }
        }

        private void runBtn_MouseClick(object sender, MouseEventArgs e)

        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String input = textBox1.Text;
                if (input.Trim() == "")
                {
                    MessageBox.Show("Enter a command", "ERROR");
                }

                else
                {
                    String[] eachLine;
                    String[] splitLines;
                    ArrayList currentLine = new ArrayList();
                    int i = 0;
                    //textBox1.Text.
                    if (input.ToLower().Trim() == "run")
                    {
                        splitLines = richTextBox1.Lines.ToArray(); //splits all lines in the rich text box and stores into an array
                        while (splitLines.Length != i)
                        {
                            eachLine = splitLines[i].Split(' ');
                            currentLine.Add(eachLine);
                            i++;
                        }
                    }

                    else
                    {
                        splitLines = textBox1.Lines.ToArray(); 
                        eachLine = textBox1.Text.Split(' '); 
                        currentLine.Add(eachLine);
                    }
                    InputCheck(currentLine, splitLines);
                    pictureBox1.Refresh();

                }
            }
            else
            {

            }
        }

        private void InputCheck(ArrayList currentLine, String[] lines) //method to check the users input 
        {
            using (var g = Graphics.FromImage(pictureBox1.Image))
            {
                int count = 0;
                while (lines.Length >= count) 
                {
                    try
                    {
                        string[] element = (String[])currentLine[count]; //splits the input of the user and stores it as 'elements' in an array.                      
                        switch (element[0].ToLower()) //element[0] is the case, so element[1] would be the following section of the command and so on
                        {
                            case "circle":
                                int radius;
                                if (!int.TryParse(element[1], out radius)) //if radius is not a number display error message
                                {
                                    MessageBox.Show("Not a number", "ERROR");
                                }
                                else
                                {
                                    new Circle(red, x, y, radius).draw(g); //draw the circle
                                }

                                break;

                            case "rectangle":
                                int width;
                                int height;
                                if (!int.TryParse(element[1], out width) || !int.TryParse(element[2], out height)) //if height and width are not numbers display error message
                                {
                                    MessageBox.Show("Not numbers", "ERROR");
                                }
                                else
                                {
                                    new Rectangle(blue, x, y, width, height).draw(g); //draw the rectangle
                                }
                                break;

                            case "triangle":
                                int s1, s2, s3;
                                if (!int.TryParse(element[1], out s1) || !int.TryParse(element[2], out s2) || !int.TryParse(element[3], out s3)) //if the 3 points are not numbers display error message
                                {
                                    MessageBox.Show("Not numbers", "ERROR");
                                }
                                else
                                {
                                    int.TryParse(element[1], out s1);
                                    int.TryParse(element[2], out s2);
                                    int.TryParse(element[3], out s3);
                                    Point sidea = new Point(s1, s2); //create the sides of the triangle by joining two points together
                                    Point sideb = new Point(s2, s3);
                                    Point sidec = new Point(s3, s1);
                                    Point[] sides = //store the 3 sides into and array to create a triangle 
                                    {
                                        sidea, sideb, sidec
                                    };

                                    new Triangle(yellow, x, y, sides).draw(g); //draw the triangle
                                }
                                break;

                            case "drawto":
                                Pen myPen = new Pen(Color.Black, 2);//create a pen object
                                int a;
                                int b;
                                if (!int.TryParse(element[1], out a) || !int.TryParse(element[2], out b))
                                {
                                    g.DrawLine(myPen, x, y, 10, 10);
                                }
                                else
                                {
                                    g.DrawLine(myPen, x, y, a, b); //use the DrawLine method
                                }
                                break;

                            case "moveto":
                                {
                                    this.x = int.Parse(element[1]);
                                    this.y = int.Parse(element[2]);
                                    MessageBox.Show("Pen position moved", "Success");
                                }
                                break;

                            case "clear":
                                g.Clear(Color.Transparent); //clears the graphics, disposes of the pen and clears the text boxes
                                g.Dispose(); 
                                textBox1.Text = null;
                                richTextBox1.Text = null;
                                break;


                        }
                        pictureBox1.Refresh(); 
                    }

                    catch
                    {

                    }
                    count++;
                }
            }
            
        }

        private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {             
 
        }

        private void button1_Click(object sender, EventArgs e) //method to load file
        {
            OpenFileDialog openFile1 = new OpenFileDialog();
            openFile1.DefaultExt = "*.txt";
            openFile1.Filter = "TXT Files|*.txt";
            if (openFile1.ShowDialog() == System.Windows.Forms.DialogResult.OK && openFile1.FileName.Length > 0) 
            {
                richTextBox1.LoadFile(openFile1.FileName, RichTextBoxStreamType.PlainText);
            }
            openFile1.Dispose();
        }

        private void button2_Click(object sender, EventArgs e) //method to save file
        {
            System.IO.MemoryStream userInput = new System.IO.MemoryStream(Encoding.UTF8.GetBytes(richTextBox1.Text));

            SaveFileDialog save = new SaveFileDialog();
            save.DefaultExt = "txt";
            save.Filter = "Text files (.txt)|.txt|All files (.)|*.*";
            DialogResult result = save.ShowDialog();
            Stream fileStream;

            if (result == DialogResult.OK)
            {

                fileStream = save.OpenFile();
                userInput.Position = 0;
                userInput.WriteTo(fileStream);
                fileStream.Close();
                userInput.Close();

            }
            save.Dispose();
        }
    }
}
