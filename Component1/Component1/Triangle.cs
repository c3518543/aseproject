﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component1
{
    class Triangle : Shape
    {
        Point[] side = new Point[3]; //this is different from shape; An array of 3 points to form the triangle

        public Triangle(Point[] side) 
        {
            this.side = side;
        }

        public Triangle(Color colour, int x, int y, Point[] side) : base(colour, x, y)
        {
            this.side = side;
        }
        public override void draw(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            Brush b = new SolidBrush(colour);
            g.DrawPolygon(p, side);
            g.FillPolygon(b, side);
            p.Dispose();
            b.Dispose();
        }
    }
}

